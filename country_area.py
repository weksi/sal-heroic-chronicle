'''
    Author: Ville-Veikko Vähäaho
    This module contains a function that
    draws home area type: city or countryside.
    It also draws home county for the characters from 
    the Country called Venestaas.
    Venestaas is currently only country we have counties for but this code can 
    be upgraded later when counties are added for other countries.
'''
import file_opener
import randomizers
from variables import directory, randomize_home_area_type

# List of area types. (City or Countryside)
home_area_type = ["City", "Countryside"]
# Function draws characters home area type and home county IF character 
# is from Venestaas
def home_area(home):
    if home == "Venestaas":
        results, weights = file_opener.OpenWeighedFiles(
                        directory + "hc_venestaas_area.txt")
        randomize_county = randomizers.weighed_randomizer(results, weights, 1)
        # randomize_home_area_type is used also in family functions 
        # so it is in variables file
        randomize_home_area_type.extend(randomizers.non_weighed_randomizer(
                                        home_area_type, 1))
        return randomize_county + randomize_home_area_type
    else:
        randomize_home_area_type.extend(randomizers.non_weighed_randomizer(
                                        home_area_type, 1))
        return randomize_home_area_type

