''' 
  Author: Ville-Veikko Vähäaho
  This module contains function to divide weighed files contents and 
  dividing them to two separate lists for other functions to use. 
'''
# List "weights" is for random weights for random generator
# List "results" is for list of results for random generator
def divide_lines(workfile):
    results = []
    weights = [] 
    for line in workfile:            
        # Trim reduntant line endings like /r/n from end of the lines.
        dividelist = line.rstrip().split("*")
        results.extend([dividelist[1]])
        weights.append(int((dividelist[0])))
    return results, weights
