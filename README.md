# Automatic SAL Heroic Chronicle
Kehittäjä: Ville-Veikko Vähäaho<br>
Automatic SAL Heroic Chronicle on Pöytäroolipelihahmon taustatarinaelementti generaattori.<br>
Ohjelma arpoo erilaisia elementtejä hahmolle kuten esimerkiksi kotimaan ja perheen koon, joista pelaaja ja pelinjohtaja(Dungeon Master, DM) voivat ottaa inspiraatiota hahmoa/hahmoja kehitellessään. HUOM! Elementtejä ei tarvitse käyttää sellaisenaan vaan niitä voi muokata, mutta muista aina jutella asiasta pelinjohtajasi kanssa.<br>
Tämä kyseinen versio on räätälöity kehittäjän omalle peliporukalle. Data on otettu porukan pelinjohtajien yhdessä kehittämästä Heroic Chronicle -taulukosta.<br>
Ohjelmaa on helppo muuttaa myös muiden peliporukoiden tarpeisiin muuttamalla datakansion tiedostoja.<br>

Aikaisemmin peliporukkamme käytti sheets taulukkoa, josta elementit arvottiin yksi kerrallaan noppaa käyttäen, mutta näitä taulukoita oli hankala muokata nopankäyttö vaateen vuoksi ja se vei liikaa aikaa. Nyt tämä ohjelma arpoo elementit puolestamme.

# Asennusohjeet

Et tarvitse Pythonin lisäksi muita lisäosia.<br>
Kansion rakenne pitäisi olla seuraava, että ohjelma toimii.<br>
harjoitustyo -kansio<br>
![Pääkansio](mdimages/folder_structure.png "Tältä pääkansion kuuluisi näyttää.")<br>
harjoitustyo/data -kansio<br>
![Datakansio](mdimages/data_files.png "Tältä datakansion kuuluisi näyttää.")


# Ohjelman käynnistäminen

Luo uusi kansio, johon haluat tallentaa Heroic Chronicle -ohjelman.<br>
Aja siihen kansioon git clone annetusta tästä repositoriystä.<br>
Avaa sen jäljeen komentorivi ja siirry kansioon, jonka juuri loit.<br>
Siirry kansiorakenteessa eteenpäin harjoitustyo kansioon. (ttc2030 --> harjoitustyo)<br>
Suorita komentorivillä "python heroic_chronicle.py" komento.

# Käyttöohjeet

Tarvitset ohjelmaa varten koneellesi python kääntäjän. Voit ladata sellaisen https://www.python.org/ <br>
Ohjelma pyytää ensin antamaan tiedostolle nimen, johon arvottu data tallennetaan. Voit käyttää tiedostonnimessä vain kirjaimia a-z, A-Z, numeroita, väliviivoja, alaviivoja ja välilyöntejä. Tiedosto päätettä ei tarvitse lisätä perään. Voit jättää kohdan myös tyhjäksi, jolloin tiedoston nimeksi tulee "heroic_chronicle.txt"<br>
Sen jälkeen ohjelma pyytää antamaan Oman nimesi. Voit jättää kohdan myös tyhjäksi.<br>
Tämän jälkeen ohjelma pyytää antamaan Pelinjohtajan nimen, kenen peliin olet osallistumassa. Voit jättää kohdan myös tyhjäksi.<br>
Tämän jälkeen ohjelma pyytää keksimään hahmollesi nimen. Voit jättää kohdan myös tyhjäksi.<br>
Näiden jälkeen ohjelma arpoo sinulle taustatarina elementtejä ja tallentaa ne "heroic_chronicle" nimiseen kansioon käyttäjäsi työkansioon. Windows 10:ssä tämä on C:\Users\windowskäyttäjänimi. Jos kansiota ei vielä ole siellä se luo sellaisen.<br>
Jos kaikki meni hyvin, ruutuun pitäis ilmestyä teksti "File was created succesfully!"

# Tekstitiedoston selitykset

Seuraavassa käydään läpi tekstitiedoston kohtia.

## Alku

Alussa pieni huomautus, että mikään ei ole kiveen hakattua vaan näistä on tarkoitus vain vetää inspiraatiota. Tämän jälkeen tiedostosta löytyvät käyttäjän antamat erittelytiedot eli pelaajan, gm:n ja hahmon nimet.

## Home Area

Home area kertoo mistä hahmo on kotoisin ja minkälaiselta alueelta hän on, joko kaupungista tai maaseudulta. Lisäksi, jos hahmo on kotoisin Venestaasin valtiosta on hänelle merkitty myös kotimaakunta.

## Birthday and Favorite food

Tässä kohtaa näkyy minä päivänä ja missä kuussa hahmo on syntynyt. Lisäksi hahmolle arvottiin myös lempiruoka perinteisistä maailman herkuista.

## Family

Family kohdassa näkyy minkälainen perhe hahmolla on tai on ollut. Lisäksi Family relationships kohdassa näkyy minkälaisia vahvoja suhteita hänellä on joihinkin perheen jäseniinsä. Näitä suhteita on jokaisella hahmolla sattumanvaraisesti 1-3.

## Allies

Sen jälkeen hahmolle on arvottu liittolaisia. Niitä voi olla 0-3. Jokaisella liittolaisella on omanlaisensa yhteys hahmoon. Lisäksi jokaisella liittolaisella on identiteetti, joka antaa ideaa millainen hahmo on. 

## Rivals

Jokaisella kunnon hahmolla on myös kilpailijoita/vastustajia. Niitä voi olla 0-3. Jokaisella kilpailijalla/vastustajalla on omanlaisensa yhteys hahmoon. Lisäksi jokaisella kilpailijalla/vastustajalla on identiteetti, joka antaa ideaa millainen hahmo on. 

## Fateful Moments

Jokainen hahmo on kokenut jotain, joka on muuttanut heidän elämänsä suunnan. Sitä kuvataan näillä fateful momenteilla. Jokainen hahmo saa ainakin yhden fateful momentin, mutta niitä voi olla enemmänkin riippuen minkälaisia liittolais/vastustaja identiteettejä hahmo saa. Fateful momentit antavat myös mekaanisia asioita hahmolle, kuten uusia loitsuja tai kykyjä.

## Mysterious Secrets

Jokainen hahmo on kokenut jotain kummallista elämänsä aikana. Nämä ovat täysin taustarinallisia "kummallisuuksia" eikä niillä ole mekaanista vaikutusta hahmon kehityksessä.