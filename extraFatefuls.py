''' 
  Author: Ville-Veikko Vähäaho
  On default character has one fateful moment,
  but character can have more depending on your ally and rival_identities.
  This module contains a function that determines, 
  if characates has more than one.
'''
import file_opener
from variables import directory

def extraFatefulsAmount(identity_list):
    sum_of_identities = 0
    # Program needs the list of the identities that give extra fateful moments
    extraFateful_identities = file_opener.OpenNonWeighedFiles(
                      directory 
                      + "hc_extra_fateful_identities.txt")
    # to compare it with drawn identities
    for i in range(len(identity_list)):
      for x in range(len(extraFateful_identities)):
        if(identity_list[i]) == extraFateful_identities[x]:
            sum_of_identities += 1
            break
    return sum_of_identities
