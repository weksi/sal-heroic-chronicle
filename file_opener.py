''' 
  Author: Ville-Veikko Vähäaho
  This module contains functions opens the data files. 
  There are two kinds of data: Weighed and Non Weighed.
'''
import codecs
import line_divider

# Function to open files that have weights and results in them 
# and ask another function to format those files for our use
def OpenWeighedFiles(filename): 
    try:  
        with codecs.open(filename, "r", encoding="utf-8") as file:
            # Weighs and results need to be separated so this function call is 
            # for that.
            weights, results = line_divider.divide_lines(file)
            return weights, results
    except:
        print(f"File {filename} not found or can't be opened.")
# Function to open files that have only results in them 
# and format those results for our use.
def OpenNonWeighedFiles(filename):
    try:   
        with codecs.open(filename,"r", encoding="utf-8") as file:
            # Trim reduntant line endings like /r/n from end of the lines.
            results = [line.rstrip() for line in file.readlines()]
            return(results)
    except:
        print(f"File {filename} not found or can't be opened.")