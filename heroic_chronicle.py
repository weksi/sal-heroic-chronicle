'''
    author: Ville-Veikko Vähäaho
    Automatic SAL Heroic Chronicle.
    This is the main file of the program.
    Heroic Chronicle is a backgrounds story element generator.
    This program is made for our own Dungeons and Dragons group(SAL) 
    but can be easily extented for other groups also.
    Purpose of this program is to draw backstory elements
    for different areas of the Dungeons and Dragons characters life.
    We used to have it on google sheets and use physical dice, 
    but the Heroic Chronicle has gotten too big for individual rolls. 
    This program also makes it easier 
    to expand our pools where we draw elements from.
'''
import random
import re
import file_opener
import randomizers
import country_area
import family_functions
import connections_amount
import extraFatefuls
import file_writer
from variables import writelist, directory

#Introduction and instructions for the program.
print("This program draws the background story elements for you.")
print("It is based on SAL Heroic Chronicle made by SAL Dungeon Masters")
print("The Heroic Chronicle automatizer will save \
background textfiles into \"Heroic Chronicle\" folder \
in your Users home direcetory. \
If that folder does not exist yet it will create one.")

# Ask the user what would he like to name the file.
file_name = input("Set the name of the file you want to save\
<You don't need to add file ending.>: ") + ".txt"
# If characters that are not allowed in file name are given, raise an exception.
if not re.match("^[\w\-. ]+$", file_name):
    raise Exception("Give a valid file name. Only numbers, letters a-z or A-Z,\
whitespaces, hyphens and underscores allowed.")  
# if user leaves file name empty program will use placeholder name for it 
# and inform the user.
if file_name == ".txt":
    file_name = "heroic_chronicle.txt"
    print("Your file will be saved as \"heroic_chronicle.txt\"")
# Ask the user what is their name
your_name = input("What is your name?: ") 
# Ask the user what is the name of the DM of the game he is attending.
gm_name = input("Who is the Dungeon Master? ")   
# Ask the user what he would like to name the character
character_name = input("What is the name of your character: ")
# writelist is global variable (found in variables.py), 
# that the program stores the data that is later saved in the text file.
# Add little disclaimer to the enduser in the file.
writelist.append("DISCLAIMER: Remember that you don't need \
to use these results as they are. Talk with your Dungeon Master\
about the changes you would like to make.") 
# Add the players, gm:s and characters names to the writelist.
writelist.append("Player: " + your_name) 
writelist.append("DM: " + gm_name) 
writelist.append("Character: " + character_name) 

'''
HOME AREA
Everyone is from somewhere. Heroic Chronicle -program draws a home country 
and wheter character is from the city or Countryside.
If the character is from Venestaas which is the main setting place 
for the campaings at the moment. Character also gets a home county.
'''
# Add empty row and title for the next part of the file. 
# New line is at the start of the string for extra emptyline 
# because of requested formatting of the file.
writelist.append("\nHOME AREA:") 
# Getting needed pool of results and weights for the randomizers from the file.
home_results, home_weights = file_opener.OpenWeighedFiles(
                            directory +  "hc_home_country.txt")
# Setting variables for homecountry and calling functions to pick randomly one. 
# If the file has weighed results, the program is made to use weighed_randomizer
# function from randomizers module
home_country = randomizers.weighed_randomizer(home_results, home_weights, 1)
# Adding drawn result to writelist.
writelist.extend(home_country)
# Calling function to randomize homearea or and home area type
# If the home country is Venestaas program 
# will also draw you a county you are from. 
# Otherwise program will only draw home are type: City or Countryside.
home_area = country_area.home_area(home_country[0])
writelist.extend(home_area)

'''
Birthday and Favorite food
Each character has a birthday.
Program also draws a favorite food for each character.
'''
# Add two empty rows and title for the next part of the file.
writelist.append("\n\nBIRTHDAY AND FAVORITE FOOD:")
# Getting needed pool of results for the randomizers from the file.
# if the file does not contain weighed item pool, 
# the program is made to use nonweighed fileopener and randomizers. 
birthday_results = file_opener.OpenNonWeighedFiles(
                directory + "hc_birthday.txt")
# Setting variables for birthday 
# and calling functions to pick randomly a birth date.
birthday = randomizers.non_weighed_randomizer(birthday_results, 1)
writelist.extend(birthday)
# Setting variables for favorite food 
# and calling functions to pick randomly a favorite food.
favorite_food_results = file_opener.OpenNonWeighedFiles(
                    directory + "hc_favoritefood.txt")
favorite_food = randomizers.non_weighed_randomizer(favorite_food_results, 1)

writelist.extend(favorite_food)

'''
FAMILY
Everyone has or had a family. Even the sad rogues who claim they are orphans. 
It doesn't matter if the family is your biological or found later in your life.
In Heroic Chronicle characters get the amount of living family members. 
Each character also have 1 to 3 strong family connections which determine your 
relationship to some of your family members.
Even if the character has 0 living family members they can still have strong 
family connections with for example extend family.
'''
# Add empty row and title for the next part of the file.
writelist.append("\nFAMILY:") 
# Setting variables for number of parents and siblings and calling functions 
# to draw the amount of parents and siblings you have. 
number_of_parents = family_functions.parents()
number_of_siblings = family_functions.siblings()
# Adding drawn results to writelist
writelist.extend(number_of_parents)
# Adding drawn results to writelist
writelist.append(number_of_siblings)
# Add title for the next part of the file.
writelist.append("FAMILY RELATIONSHIPS:") 
# Setting variables for strong familyrelationships and calling functions 
# You can have 1 to 3 strong family relationships. 
family_relationship_results = file_opener.OpenNonWeighedFiles(
                            directory + "hc_family_relationships.txt")
family_relationships_amount = random.randint(1,3)
family_relationships = randomizers.non_weighed_randomizer(
                    family_relationship_results, 
                    family_relationships_amount)
# Adding drawn result to writelist
writelist.extend(family_relationships)

'''
ALLIES AND RIVALS
Each character can have any number of allies and rivals between 0 and 3. 
All of those allies and rivals have some sort of connetion to the character.
Each ally and rival also has an identity. 
Most of them are from DnD 5e Monster Manual but we added few of our own.
'''
# Add empty row and title for the next part of the file.
writelist.append("\n\nALLIES:") 
# Character can have allies. First program draws how many of them there are.
# Setting variables for amount of allies
# and calling functions to draw the number of allies. 
allies_amount_results, allies_amount_weights = file_opener.OpenWeighedFiles(
                                             directory + "hc_allies_amount.txt")
alliesAmount = randomizers.weighed_randomizer(
            allies_amount_results, allies_amount_weights, 1)
# Adding drawn result to writelist
writelist.extend(alliesAmount)
# Program needs number of allies in integerform for number of ally connections 
# and number of ally identities.
allies_amount_int = connections_amount.amount_of_connections(alliesAmount[0])
# Allies have certain type of connection to the character. 
# Program draws one for each ally.
# Program needs ally_identity variable for later use, 
# but it can be empty if there are no allies.
ally_identity = [] 
if allies_amount_int != 0:
    ally_connection_results = file_opener.OpenNonWeighedFiles(
                                directory + "hc_ally_connections.txt")
    ally_connections = randomizers.non_weighed_randomizer(
                        ally_connection_results, 
                        allies_amount_int)
    # Each ally has certain kind of identity. 
    # Most of them are from DnD 5e Monster Manual.
    ally_identity_results, ally_identity_weights = file_opener.OpenWeighedFiles(
                                        directory + 
                                        "hc_ally_and_rival_type.txt")
    ally_identity = randomizers.weighed_randomizer(
                    ally_identity_results, 
                    ally_identity_weights,  
                    allies_amount_int)
    # Add title for the next part of the file.
    writelist.append("ALLY CONNECTIONS:")
    # Adding drawn results to writelist
    writelist.extend(ally_connections)
    # Add title for the next part of the file.
    writelist.append("ALLY IDENTITIES:")
    # Adding drawn results to writelist
    writelist.extend(ally_identity)

# Add empty row and title for the next part of the file.
writelist.append("\nRIVALS:") 
rivals_amount_results, rivals_amount_weights = file_opener.OpenWeighedFiles(
                                            directory + "hc_rivals_amount.txt")
# Setting variables for amount of rivals 
# and calling functions to draw the number of rivals. 
rivals_amount = randomizers.weighed_randomizer(rivals_amount_results, 
                rivals_amount_weights, 1)
# Adding drawn result to writelist
writelist.extend(rivals_amount)
# Rivals have certain type of connection to the character. 
# Program draws one for each rival.
# We need number of rivals in numberform number of rivalconnections 
# and number of rival types.
# Program needs rival_identity variable for later use, 
# but it can be empty if there are no rivals.
rivals_amount_int = connections_amount.amount_of_connections(rivals_amount[0])
rival_identity = []
if rivals_amount_int != 0:
    rival_connection_results = file_opener.OpenNonWeighedFiles(
                                directory + "hc_rival_connections.txt")
    rival_connections = randomizers.non_weighed_randomizer(
                        rival_connection_results, rivals_amount_int)
    # Each rival has certain kind of identity. 
    # Most of them are from DnD 5e Monster Manual.
    rival_type_results, rival_type_weights = file_opener.OpenWeighedFiles(
                                            directory 
                                            + "hc_ally_and_rival_type.txt")
    rival_identity = randomizers.weighed_randomizer(
                    rival_type_results, 
                    rival_type_weights, 
                    rivals_amount_int)
    # Add title for the next part of the file.
    writelist.append("RIVAL CONNECTIONS:")
    # Adding drawn results to writelist
    writelist.extend(rival_connections)
    # Add title for the next part of the file.
    writelist.append("RIVAL TYPES:")
    # Adding drawn results to writelist
    writelist.extend(rival_identity)

'''
FATEFULMOMENTS
Fatefulmoments are turning points in the characters life. 
Something big that has happened. 
Every character has at least 1 fatefulmoment, 
but they can have more depending on what kind of ally 
and rival identities they got.
'''
# Add empty row and title for the next part of the file.
writelist.append("\n\nFATEFUL MOMENTS:")
# Amount of extrafatefuls depends on how many of certain type identities 
# character has in allies and rivals. 
# The types can be found in the file hc_extra_fateful_types.txt.
extra_f_allies = extraFatefuls.extraFatefulsAmount(ally_identity)
extra_f_rivals = extraFatefuls.extraFatefulsAmount(rival_identity)
amount_of_extrafatefuls = extra_f_allies + extra_f_rivals + 1
fateful_moment_results = file_opener.OpenNonWeighedFiles(
                        directory + "hc_fatefulMoments.txt")
fateful_moments = randomizers.non_weighed_randomizer(
                fateful_moment_results, amount_of_extrafatefuls)
# Adding drawn result to writelist
writelist.extend(fateful_moments)

'''
MYSTERIOUS SECRETS
Mysterious secrets are weird events in your life that you barely remember. 
These are for Dungeon Master and player to draw inspiration.
Each character has 1 mysterious secret.
'''
# Add empty row and title for the next part of the file.
writelist.append("\n\nMYSTERIOUS SECRETS:") 
mysterious_secrets_results = file_opener.OpenNonWeighedFiles(
                            directory + "hc_mysterious_secret.txt")
mysterious_secret = randomizers.non_weighed_randomizer(
                  mysterious_secrets_results, 1)
# Adding drawn result to writelist
writelist.extend(mysterious_secret)

# Calling function to write writelist contents into a file. 
# File_name is the name that user gave 
# or "heroic_chronicle.txt" if field was left empty. 
file_writer.write_to_file(file_name)