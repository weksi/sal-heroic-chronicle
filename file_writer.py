''' 
  Author: Ville-Veikko Vähäaho
  This module contains function that writes data the program has drawn into a
  text file.
'''
import os
import codecs
from variables import writelist

def write_to_file(file_name):
    # Directory name for directory save location.
    save_directory = "heroic_chronicle"
    # Program saves the data in the heroic_chronicle folder 
    # in the users home directory
    directoryPath = os.path.expanduser("~") + "/" + save_directory
    # if the heroic_chronicle folder does not exists program will create one.
    if not os.path.exists(directoryPath):
        try:
            os.mkdir(directoryPath)
        except:
            print("Failed to create the directory.")
    # file_name is the one that user gave or the placeholder one thatif the 
    # field was left empty.
    filePath = directoryPath + "/" + file_name
    try:
        # Using utf-8 to make sure germanic and skandinavian letters work.
        file = codecs.open(filePath, "w", "utf-8")
        # Write the contents of the writelist into the file and adding newline
        # at the end of the line.
        for rivi in writelist:
            file.write(rivi + "\n")
        print("File was created succesfully!")
    except:
        print("Failed to create the file or write into it.")
    finally:   
        file.close()