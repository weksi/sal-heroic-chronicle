While traveling through [Place, Not a City], I had a waking vision that the [Constellation] which glows above [Place, Not a City] was eclipsed and began to sink into the ground.
An agent of the [Organization] once begged my mother to “reconsider their offer,” but she spat in her face and said “that’s what I think of your offer!” I’ve seen strange people watching her ever since.
I remember growing up in a dank and dismal town of bandits/pirates called [City]. My family escaped when I was a child, and we had to leave something very valuable behind. I’ve been trying to get back there ever since.
I discovered a passage to the [Forbidden/secret place] that no one knows about while exploring in the woods. I see it in my dreams on strange nights—worse, I sometimes see things coming out of it, or climbing into it.
While sleeping in an inn in [City], I overheard a conversation through my window about a gold dragon living somewhere beneath the city. That can't be right...
A cleric of [God] once sheltered in my home for the night. My parents let him stay. When I awoke, he was gone—and when I asked my parents when he had left, no one remembered his arrival.
I met a priest of [God] once on the streets of [City]. I was quickly pulled away, but not before she cackled and pointed at me, saying, “In time, the curse of death will claim you. It will claim everything!”
I stole from a beautiful [City] nobleman as a child, and later discovered that among the things I stole was his signet ring. I’ve seen wanted posters seeking the thief my entire life.
I lived for a year in [City] in [Place]. One day while living there, a wild-eyed man grabbed me by the collar and shook me, demanding that I tell him where to find the Ring of Winter. His friend slapped his hand and told him I wasn’t the one he was looking for, and they quickly vanished down an alley.
One night, I discovered a golden coin with the face of a beautiful lady on one side. In the morning, the face was gone, and a leering, demonic face was there in its place. Whenever I try to get rid of it, I wake up with it in my pocket.
I was given a card from a mysterious street performer. It’s in black and white with a disturbing illustration. The card is labeled The [Tarokka card].
I was saved from an angry [Animal1] as a child when a giant [Animal2] leapt in front of me. After it scared the [Animal1] off, the [Animal2] turned to look at me with such compassion in its eyes, I knew it must be a forest spirit, or even a druid in an animal’s form.
I once overheard my parents saying that, when I was born, my father saw a group of [people] with a strange mark on their foreheads gazing in through the window. I’ve seen that mark out of the corner of my eye my entire life, always disappearing when I look closely for it.
As a child, I once saw my father meet with a man in full plate armor and a face-covering helm, who called himself [Interesting last name]. When I asked him who that was, he told me he was an old friend and quickly changed the subject. I’ll never forget the look of that armored man.
I have a vague memory that I joined a cult when I was young, and I have a tattoo shaped like the cult’s symbol on my bicep. I can’t remember anything else about it, and my head hurts when I try to remember it.
I was inducted into the Monsterjägers when I was young, and I’ve been keeping an eye on the monsters near my hometown for years.
I found a dragon egg in the forest when I was young. I figure it must be a fake, since it’s never hatched over all these years. But I keep it safe and secret anyway.
While traveling along the Trade Way, I discovered a book from Star of Mages lying in the mud. No one else could see it, and when I opened it, all the pages were blank (save for the mark of [God / Monastery] on its inner cover). I’ve kept the book, but have never written in it.
I’ve heard the sound of waves crashing in my dreams for the past five years. Recently, they’ve gotten louder, and I can hear a sinister voice quietly speaking in a language I can’t understand underneath the sound of waves.
A paladin in the service of the Order of the [Order] killed my uncle for worshiping the evil being. To this day, I don’t know if he truly worshiped dark forces or not.
Years ago, my best friend came to me in the middle of the night and gave me a key that glowed with an icy blue light. I never saw that friend again.
I was the only witness to a cold-blooded murder. In the aftermath, I saw the killer take a gold coin with a ruby inlaid at its center from the victim’s body.
I once had a dream where an old stranger looked me dead in the eye, screamed “Scourger!” at the top of their lungs, and then exploded into a column of flame.
While exploring near my home, I found a cliff with a bunch of caverns dug out of it, all of them large enough for people to hide within.
I once saw a cat that seemed to be moving with a strange sense of purpose. I followed it to the dwelling of an important local elder, where it gazed through the window for an hour. Then it suddenly shook itself and raced away, as though a spell had been broken.
One of my parents left home in the middle of a storm, in the middle of the night. They had their sword and shield. They came back a week later, with the shield practically in pieces. They never talked about that night.
I had a friend who was a farmer. Every week, their crops doubled in size until they had pumpkins as big as houses. Then the next week, the friend was gone and their fields were torched. I never heard from them again.
I once saw an enormous figure walking through the clouds on a stormy night. At one point, they looked at me, and then kept walking.
I woke up one night to find one of my siblings perched on my chest, staring into my eyes. They said, “The time is soon,” and then giggled and ran off. When I asked them, they had no memory of the event.
I once saw a giant bird soar past overhead. It croaked out a cry that sounded like my name, then disappeared beyond the clouds.
I ate a fruit whose seeds spelled out a magic word where I’d cast them onto the ground. Years later, I saw the same word spelled out within a slice of bread.
A warrior friend of mine died. But every so often, I swear I see that friend in their old armor, at the corner of my vision.
An old seer once touched my forehead and gave me a vision of a flaming bird chained beneath a mountain, squirming and wailing in the darkness. (Tämä profetia on tavallaan toteutunut)
Once while on a boat, I heard a voice rumbling around me. I looked down below the water, and I swear I saw golden eyes looking up at me.
While I was eating, a whole potato exploded into worms, and I suffered a vision of a gigantic worm eating the world like a giant apple.
While picking flowers, I saw a tall figure with red skin and horns wandering the meadow. The flowers grew taller where they walked, but I fled in fear and never saw the figure again.
I was attacked by wolves in the woods one day, and was saved by a stranger with a bandage over their eyes. This person shone with silver light and was covered with scars—and I think I might have seen black wings tucked in at their back.
While trying to forge a sword, I accidentally burned myself with the red-hot blade. A strange vision then came to me, of powerful weapons calling out for me to wield them.
I once met someone fleeing through the woods who said they had escaped from some evil place. I asked what that meant, but the stranger fell dead on the spot. When I returned with help to collect the body, it was gone.
I once caught a falling star. It looked up from my hands and smiled, then told me to look for it on the day when fire erupts from the earth.