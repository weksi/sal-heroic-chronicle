'''
    Author: Ville-Veikko Vähäaho
    Each of your allies and rivals have connections and identities. 
    For those program needs the number of the allies and rivals.
    This module has function 
    that returns the number of drawn allies and rivals as an integer.
'''
# Function searches from the string the number of connections and returns it as integer 
# for use in ally and rival connectionn and identities.
def amount_of_connections(amount):
    if "No" in amount:
        return 0
    if "One" in amount:
        return 1
    if "Two" in amount:
        return 2
    if "Three" in amount:
        return 3
    else: 
        raise Exception("Something went wrong.")