'''
    author: Ville-Veikko Vähäaho
    this module contains functions that draw item 
    or multiple items from the given pool randomly.
'''
import random

# picks randomly weighed result(s) 
# from the given file and adds it to the writelist
# Function needs pool of results, weights 
# and number of items that will be drawn as arguments.
# random choices can return same result twice fom same draw.
def weighed_randomizer(results, weights, amount):
    return random.choices(results, cum_weights=weights, k=amount)

# Picks randomly result(s) from the given file and adds it to the writelist
# Function needs pool of results and number 
# of items that will be drawn as arguments.
# Random sample can't return same result twice from same draw.
def non_weighed_randomizer(results, amount):
    return random.sample(results, k=amount)