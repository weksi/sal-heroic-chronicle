This person believes you murdered their sibling. Regardless of your guilt or innocence, they are out for your blood.
You bested this person in combat, but they believe you cheated to defeat them. They long to prove that they are the superior warrior.
You broke a promise to this person, and it caused them to suffer greatly. Now they conspire to make someone else break a valuable promise to you.
You once loved this person, but broke their heart. They are now obsessed with making you feel the same pain they felt.
This person at least was better than you, when you two were young, and oh boy, they love to remind you about it. All. The. Time. ALL. THE. FUCKING. TIME. (also, he has a convertible and a cheer squad)
This person was ordered to arrest you, and doggedly hunts you wherever you go.
This person thinks that you were replaced by a doppelganger or possessed by a spirit or monster. They are now trying to defeat you, so as to find or free the original you.
You fled from your home under mysterious circumstances. This person is obsessed with finding out the truth of what caused you to leave.
You and this person tried to harness power beyond your control, and it left them disfigured and in constant pain. Having since mastered the power that nearly destroyed them, they now seek to turn it upon you.
You helped this person out once when they were down on their luck, and now they go to you whenever they need help.
This person wants to be your friend, but their help has always made your life harder.