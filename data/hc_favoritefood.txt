Paella—a working-class dish made with rice, white beans, and seafood
Plantain cups—a sweet and savory dish of fried plantains stuffed with meat and rice
Gazpacho—a cold soup served on hot days, made from pounded vegetables and fruit
Honeyflame bread—a fried dessert soaked in honey and coated in Capranian spices
Fusaka fish—seafood cutlets smothered in Bacrian fusaka spice and fried in oil
Snakelocks noodles—sea anemone tendrils coated in honey batter and delicately fried
Queen’s water—a soft drink made from syrup, honey, guava, and tamarind
Blacksand coffee—a tiny shot of coffee, brewed atop red-hot sand, Marquet-style
Dumplings—a steamed potato bread that can be served with any meal
Sauerbraten—a Zemnian peasant dish of pickled horse meat served with cabbage
Brawn, also known as head cheese—a meat jelly made from boiled calf’s head
Schweinshaxe—a Zemnian peasant dish of long-marinated roasted pork knuckle
Dampfnudel—a regal steamed roll served in sweet custard or with savory potatoes
Spanferkel—an expensive dish of suckling pig, roasted and served at royal parties
Trost—a sweet, dark ale brewed in Trostenwald
Radler—a sweet, expensive drink made from imported lemonade mixed with beer
Imperial pickled plums, smuggled from the Dwendalian Empire by Myriad agents
Charred venison and roasted potatoes, prepared with local game and local tubers
Raw venison still dripping with blood
Elf-mash—a creamy dish made from overripe cloudberries
Dwarven rootbake—a hearty casserole of roots and tubers wrapped in seaweed
Jam porridge—made from Xhorhasian rice and topped with salmonberry jam
Blazing tea—a beverage blended from fermented fireroot and mouth-scalding spices
Sbiten—a drink made from honey and spices, best enjoyed hot on snowy days
Rzukaal—a dish made from sautéed rice noodles, hearty mushrooms, and giant spider legs
Yuyandl—grilled yuyo (a zucchini-like vegetable that grows in Rosohna’s sunless gardens) spiced and served with rice
Mastodon kor’rundl—grilled mastodon served with sunless kor’run (a squash-like vegetable that grows in Rosohna’s sunless gardens) and rice
Kinespaji spaaldl—soup made from mushrooms or vegetables and the boiled spit of a horizonback turtle
Umarindaly—a dessert akin to rice pudding, topped with spiced gooseberry jam
Keltaly—heavy cream mixed with pulverized black currants and frozen into a fluffy, sweet, creamy dessert
Erzfaalyu—a potent spirit made from fermented rice
Yunfaalyu—a fragrant plum liquor served at frigid temperatures and garnished with currants
Mämmi
Kalakukko
Vispipuuro
Mustamakkara
Maksalaatikko (Rusinat/Ei)
Atomi
Vety
Silakkapihvit
Mustikkapiiras
Leipäjuusto
Poronkäristys
Abranpiirakka
Rönttönen
Abra-kalja (takaisin tuoppi kerrallaan)
Rössypottu
Sultsina
Porilainen
Lörtsy
Hatsapuri